CREATE SCHEMA "puppet";

CREATE TABLE "puppet"."user" (
  "id" INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(128) NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "puppet"."permission" (
  "id" INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  "name" VARCHAR(45) NOT NULL,
  "description" VARCHAR(128) NOT NULL,
  PRIMARY KEY ("id")
);

CREATE TABLE "puppet"."user_permission" (
  "user_id" INT NOT NULL,
  "permission_id" INT NOT NULL,
  PRIMARY KEY ("user_id", "permission_id")
);

CREATE TABLE "puppet"."gateway" (
	"id" INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
	"protocol" varchar(45) not null,
	"name" VARCHAR(45) NOT NULL,
	"host" VARCHAR(15) NOT NULL,
	"port" INT NOT NULL DEFAULT -1,
	PRIMARY KEY ("id")
);

-- insert default gateways
INSERT INTO "puppet"."gateway" ("protocol", "name", "host", "port") VALUES('TCP', 'Default TCP', '0.0.0.0', 9981);
INSERT INTO "puppet"."gateway" ("protocol", "name", "host", "port") VALUES('UDP', 'Default UDP', '0.0.0.0', 8864);
INSERT INTO "puppet"."gateway" ("protocol", "name", "host", "port") VALUES('HTTP', 'Default HTTP', '0.0.0.0', 7749);
