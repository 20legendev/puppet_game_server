package com.puppet.server.core.session.events;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.WorkerPool;

public class SocketReceivingController implements ExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(SocketReceivingController.class);

	private static SocketReceivingController instance = new SocketReceivingController();

	public static SocketReceivingController getInstance() {
		return instance;
	}

	private int numWorkers = 100;

	private WorkerPool<SocketReceivingEvent> workerPool;
	private RingBuffer<SocketReceivingEvent> ringBuffer;
	private ExecutorService executor;

	private SocketReceivingEventProducer producer;

	private SocketReceivingController() {

		this.executor = Executors.newFixedThreadPool(numWorkers);
		
		SocketReceivingEventHandler[] handlers = new SocketReceivingEventHandler[numWorkers];
		for (int i = 0; i < handlers.length; i++) {
			handlers[i] = new SocketReceivingEventHandler();
		}
		
		this.ringBuffer = RingBuffer.createMultiProducer(new SocketReceivingEventFactory(), 1024,
				new BlockingWaitStrategy());
		
		this.workerPool = new WorkerPool<SocketReceivingEvent>(this.ringBuffer, this.ringBuffer.newBarrier(), this,
				handlers);
		
		this.workerPool.start(this.executor);

		this.producer = new SocketReceivingEventProducer(this.ringBuffer);
	}

	public void onReceive(long sessionId, byte[] data) {
		this.producer.publish(sessionId, data);
	}

	public void shutdown() {
		this.executor.shutdown();
		try {
			if (this.executor.awaitTermination(2, TimeUnit.SECONDS)) {
				this.executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			throw new RuntimeException("error", e);
		}
	}

	@Override
	public void handleEventException(Throwable ex, long sequence, Object event) {
		logger.error("handleEventException: ", ex);
	}

	@Override
	public void handleOnStartException(Throwable ex) {
		logger.error("handleOnStartException: ", ex);
	}

	@Override
	public void handleOnShutdownException(Throwable ex) {
		logger.error("handleOnShutdownException: ", ex);
	}
}
