package com.puppet.server.core.configuration;

import com.puppet.server.core.db.AbstractBean;
import com.puppet.server.statics.Protocol;

public class GatewayConfig extends AbstractBean {

	private int id;
	private String name;
	private String host = "0.0.0.0";
	private int port;
	private Protocol protocol;

	public GatewayConfig() {

	}

	public GatewayConfig(int id, String name, String host, int port, String protocolName) {
		this.setId(id);
		this.setName(name);
		this.host = host;
		this.setProtocol(Protocol.fromName(protocolName));
		this.setPort(port);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
}
