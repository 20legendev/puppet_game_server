package com.puppet.server.core.session.events;

import static org.msgpack.template.Templates.TByteArray;
import static org.msgpack.template.Templates.TString;
import static org.msgpack.template.Templates.tMap;

import java.io.ByteArrayInputStream;
import java.util.Map;

import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.msgpack.unpacker.Unpacker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lmax.disruptor.WorkHandler;
import com.puppet.common.PuCommand;
import com.puppet.common.PuField;
import com.puppet.common.message.concrete.PongMessage;
import com.puppet.server.core.PuppetServer;
import com.puppet.server.core.session.PuSession;

public class SocketReceivingEventHandler implements WorkHandler<SocketReceivingEvent> {

	private static final Logger logger = LoggerFactory.getLogger(SocketReceivingEventHandler.class);

	private static final MessagePack msgpack = new MessagePack();

	private static PuSession getSessionById(long sessionId) {
		return PuppetServer.getInstance().getSessionManager().getSessionById(sessionId);
	}

	private static long handlerId = 0;

	private long id = ++handlerId;

	@Override
	public void onEvent(SocketReceivingEvent event) throws Exception {
		// logger.debug("handler " + id + " handling event with session id: " +
		// event.getSessionId() + ", data size: "
		// + event.getData().length + " bytes");

		PuSession session = getSessionById(event.getSessionId());
		if (session == null) {
			return;
		}

		ByteArrayInputStream in = new ByteArrayInputStream(event.getData());
		Unpacker unpacker = msgpack.createUnpacker(in);

		Template<Map<String, byte[]>> mapTmpl = tMap(TString, TByteArray);
		Map<String, byte[]> dstMap = unpacker.read(mapTmpl);

		PuCommand command = PuCommand.fromId(dstMap.get(PuField.COMMAND));

		switch (command) {
		case PING:
			// send pong message
			PongMessage pong = new PongMessage(dstMap.get(PuField.DATA));
			session.sendMessage(pong);
			break;
		case JOIN_ROOM:
			break;
		case LEAVE_ROOM:
			break;
		case LOGIN:
			break;
		case LOGOUT:
			break;
		default:
			logger.debug("command invalid: " + command);
			break;
		}
	}

	public long getId() {
		return id;
	}
}
