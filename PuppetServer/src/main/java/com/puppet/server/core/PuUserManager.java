package com.puppet.server.core;

import com.puppet.server.core.entities.PuUser;

public interface PuUserManager {
	public PuUser addUser(PuUser user);

	public boolean removeUser(String userName);

	public PuUser getUser(String userName);
}
