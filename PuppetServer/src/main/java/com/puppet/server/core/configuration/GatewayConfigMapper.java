package com.puppet.server.core.configuration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.puppet.server.statics.Fields;

public class GatewayConfigMapper implements ResultSetMapper<GatewayConfig> {

	@Override
	public GatewayConfig map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		return new GatewayConfig(r.getInt(Fields.ID), r.getString(Fields.NAME), r.getString(Fields.HOST),
				r.getInt(Fields.PORT), r.getString(Fields.PROTOCOL));
	}

}
