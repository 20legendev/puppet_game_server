package com.puppet.server.core.session.events;

import com.lmax.disruptor.EventFactory;

public class SocketReceivingEventFactory implements EventFactory<SocketReceivingEvent> {

	// private static final Logger logger =
	// LoggerFactory.getLogger(SocketReceivingEventFactory.class);

	@Override
	public SocketReceivingEvent newInstance() {
		return new SocketReceivingEvent();
	}
}
