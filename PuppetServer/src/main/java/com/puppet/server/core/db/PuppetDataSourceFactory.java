package com.puppet.server.core.db;

import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import snaq.db.DBPoolDataSource;

public class PuppetDataSourceFactory {
	private static final Logger logger = LoggerFactory.getLogger(PuppetDataSourceFactory.class);

	public static DataSource createDataSource(Properties props) throws Exception {
		if (props != null) {
			DBPoolDataSource ds = new DBPoolDataSource();
			ds.registerShutdownHook();

			for (Object keyObj : props.keySet()) {
				String refName = (String) keyObj;
				String refValue = (String) props.get(refName);
				if (refName.equalsIgnoreCase("description")) {
					ds.setDriverClassName(refValue);
					logger.trace("Set DataSource description: " + refValue);
				} else if ((refName.equalsIgnoreCase("user")) || (refName.equalsIgnoreCase("username"))) {
					ds.setUser(refValue);
					logger.trace("Set DataSource username: " + refValue);
				} else if (refName.equalsIgnoreCase("password")) {
					ds.setPassword(refValue);
					logger.trace("Set DataSource password");
				} else if (refName.equalsIgnoreCase("driverClassName")) {
					ds.setDriverClassName(refValue);
					logger.trace("Set DataSource driver class name: " + refValue);
				} else if (refName.equalsIgnoreCase("url")) {
					ds.setUrl(refValue);
					logger.trace("Set DataSource URL: " + refValue);
				} else if (refName.equalsIgnoreCase("passwordDecoderClassName")) {
					ds.setPasswordDecoderClassName(refValue);
					logger.trace("Set DataSource PasswordDecoder class name: " + refValue);
				} else if (refName.equalsIgnoreCase("validatorClassName")) {
					ds.setValidatorClassName(refValue);
					logger.trace("Set DataSource ConnectionValidator class name: " + refValue);
				} else if (refName.equalsIgnoreCase("validationQuery")) {
					ds.setValidationQuery(refValue);
					logger.trace("Set DataSource validation query: " + refValue);
				} else if (refName.equalsIgnoreCase("minPool")) {
					try {
						ds.setMinPool(Integer.parseInt(refValue));
					} catch (NumberFormatException nfx) {
						throw new NamingException("Invalid '" + refName + "' value: " + refValue);
					}
					logger.trace("Set DataSource minPool: " + refValue);
				} else if (refName.equalsIgnoreCase("maxPool")) {
					try {
						ds.setMaxPool(Integer.parseInt(refValue));
					} catch (NumberFormatException nfx) {
						throw new NamingException("Invalid '" + refName + "' value: " + refValue);
					}
					logger.trace("Set DataSource maxPool: " + refValue);
				} else if (refName.equalsIgnoreCase("maxSize")) {
					try {
						ds.setMaxSize(Integer.parseInt(refValue));
					} catch (NumberFormatException nfx) {
						throw new NamingException("Invalid '" + refName + "' value: " + refValue);
					}
					logger.trace("Set DataSource minSize: " + refValue);
				} else if (refName.equalsIgnoreCase("idleTimeout")) {
					try {
						ds.setIdleTimeout(Integer.parseInt(refValue));
					} catch (NumberFormatException nfx) {
						throw new NamingException("Invalid '" + refName + "' value: " + refValue);
					}
					logger.trace("Set DataSource idleTimeout: " + refValue);
				} else if (refName.equalsIgnoreCase("loginTimeout")) {
					try {
						ds.setLoginTimeout(Integer.parseInt(refValue));
					} catch (NumberFormatException nfx) {
						throw new NamingException("Invalid '" + refName + "' value: " + refValue);
					}
					logger.trace("Set DataSource idleTimeout: " + refValue);
				}
			}
			return ds;
		}
		return null;
	}
}