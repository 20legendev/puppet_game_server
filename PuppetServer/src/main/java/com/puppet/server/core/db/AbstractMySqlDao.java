package com.puppet.server.core.db;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public abstract class AbstractMySqlDao extends AbstractDao {
	@SqlUpdate("SET FOREIGN_KEY_CHECKS=:value")
	abstract void setForeignKeyChecks(@Bind("value") boolean value);

	@SqlUpdate("SET UNIQUE_CHECKS=:value")
	abstract void setUniqueChecks(@Bind("value") boolean value);

	@SqlQuery("SELECT LAST_INSERT_ID()")
	public abstract int getLastInsertedId();

	@SqlQuery("SHOW DATABASES")
	public abstract Object showTables();
}
