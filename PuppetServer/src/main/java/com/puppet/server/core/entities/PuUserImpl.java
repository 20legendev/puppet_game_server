package com.puppet.server.core.entities;

import com.puppet.eventdriven.Event;
import com.puppet.eventdriven.EventListener;
import com.puppet.eventdriven.impl.EventDispatcherImpl;
import com.puppet.server.core.session.PuSession;

class PuUserImpl extends EventDispatcherImpl implements PuUser, EventListener {

	private PuSession session;
	private String userName;

	PuUserImpl(PuSession session, String userName) {
		this.setUserName(userName);
		this.setSession(session);
	}

	@Override
	public String getUserName() {
		return this.userName;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public PuSession getSession() {
		return session;
	}

	public void setSession(PuSession session) {
		if (this.session != null) {
			// this.session.removeEventListener(ClientEvent.REQUEST, this);
		}
		this.session = session;
		if (this.session != null) {
			// this.session.addEventListener(ClientEvent.REQUEST, this);
		}
	}

	@Override
	@Deprecated
	public void onEvent(Event event) throws Exception {

	}
}
