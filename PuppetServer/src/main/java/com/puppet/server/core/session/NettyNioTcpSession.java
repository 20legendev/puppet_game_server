package com.puppet.server.core.session;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.common.message.PuMessage;
import com.puppet.eventdriven.Event;
import com.puppet.eventdriven.EventListener;
import com.puppet.eventdriven.impl.BaseEvent;
import com.puppet.server.core.PuppetServer;
import com.puppet.server.core.session.events.SocketReceivingController;

public class NettyNioTcpSession extends ChannelInboundHandlerAdapter implements PuSession {

	private static final Logger logger = LoggerFactory.getLogger(NettyNioTcpSession.class);

	private long id;
	private ChannelHandlerContext ctx;

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		PuppetServer.getInstance().getSessionManager().deregisterSession(this.getId());
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		logger.debug("channel active for new connection: " + ctx.name());
		this.ctx = ctx;
		this.setId(PuppetServer.getInstance().getSessionManager().registerNewSession(this));
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws IOException {
		ByteBuf bb = (ByteBuf) msg;
		byte[] bytes = new byte[bb.readableBytes()];
		bb.readBytes(bytes);
		SocketReceivingController.getInstance().onReceive(this.getId(), bytes);
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws InterruptedException {

	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		logger.error("error on channel", cause);
		// ctx.close();
	}

	private Map<String, List<EventListener>> listeners;

	@Override
	public void addEventListener(String eventType, EventListener listener) {
		if (this.listeners == null) {
			this.listeners = new ConcurrentHashMap<String, List<EventListener>>();
		}
		if (!this.listeners.containsKey(eventType)) {
			this.listeners.put(eventType, new CopyOnWriteArrayList<EventListener>());
		}
		this.listeners.get(eventType).add(listener);
	}

	@Override
	public void removeEventListener(String eventType, EventListener listener) {
		if (this.listeners != null && this.listeners.containsKey(eventType)) {
			if (listener == null) {
				this.listeners.remove(eventType);
			} else {
				this.listeners.get(eventType).remove(listener);
			}
		}
	}

	@Override
	public void removeAllEvent() {
		this.listeners = null;
	}

	@Override
	public void dispatchEvent(Event event) {
		String eventType = event.getType();
		if (eventType != null && this.listeners.containsKey(eventType)) {
			List<EventListener> tmpListeners = this.listeners.get(eventType);
			if (tmpListeners.size() > 0) {
				for (EventListener listener : tmpListeners) {
					try {
						listener.onEvent(event);
					} catch (Exception e) {
						throw new RuntimeException("error while event handled");
					}
				}
			}
		}
	}

	@Override
	public void dispatchEvent(String eventType) {
		this.dispatchEvent(new BaseEvent(eventType));
	}

	@Override
	public void sendMessage(byte[] message) {
		if (this.ctx == null) {
			throw new RuntimeException("channel context not activated");
		}
		// logger.debug("sending " + message.length + " bytes data to client");
		ByteBuf response = Unpooled.buffer(256);
		response.writeBytes(message);
		ctx.writeAndFlush(response);
	}

	@Override
	public void sendMessage(PuMessage message) {
		try {
			// logger.debug("sending " + message.getCommand() + " message");
			this.sendMessage(message.getBytes());
		} catch (IOException e) {
			throw new RuntimeException("cannot serialize message to byte array", e);
		}
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}
}
