package com.puppet.server.core;

import com.puppet.server.core.session.PuSession;

public interface PuSessionManager {
	public long registerNewSession(PuSession session);

	public PuSession deregisterSession(long id);
	
	public PuSession getSessionById(long id);
}
