package com.puppet.server.core.session.events;

public class SocketReceivingEvent {
	private byte[] data;
	private long sessionId;

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
}