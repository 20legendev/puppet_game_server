package com.puppet.server.core.configuration;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.server.core.db.PuppetDataSourceFactory;

public class ServerConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(ServerConfiguration.class);

	private static ServerConfiguration _instance = null;

	public static ServerConfiguration getInstance() {
		if (_instance == null) {
			try {
				_instance = new ServerConfiguration();
			} catch (Exception e) {
				throw new RuntimeException("error while create new server configuration...", e);
			}
		}
		return _instance;
	}

	private File dbFile = new File(System.getProperty("embeddedDbLocation", "conf/db"));
	private DataSource dataSource = null;
	private DBI dbi = null;

	private ServerConfiguration() {
		System.setProperty("derby.stream.error.file", "logs/derby.log");
	}

	private Properties buildConfigurationProperties() {
		Properties result = new Properties();
		result.setProperty("url", "jdbc:derby:" + this.dbFile.getAbsolutePath()
				+ (this.dbFile.exists() ? "" : ";create=true"));
		result.setProperty("driverClassName", "org.apache.derby.jdbc.EmbeddedDriver");
		result.setProperty("user", "puppet_server");
		result.setProperty("password", "e1b09c79aaa4122f5f5e662889cc2a7e");
		result.setProperty("idleTimeout", "3000");
		return result;
	}

	public void init() throws Exception {
		this.dataSource = PuppetDataSourceFactory.createDataSource(this.buildConfigurationProperties());
		this.dbi = new DBI(this.dataSource);
		if (!dbFile.exists()) {
			logger.info("creating new database...");
			this.createNewDatabase();
		}
	}

	private void createNewDatabase() {
		try (Handle handle = this.dbi.open()) {
			handle.createScript("puppet.sql").execute();
		}
	}

	private ServerConfigurationDao getDao() {
		return this.dbi.open(ServerConfigurationDao.class);
	}

	public List<GatewayConfig> getAllGateway() {
		try (ServerConfigurationDao dao = this.getDao()) {
			return dao.getAllGateway();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
