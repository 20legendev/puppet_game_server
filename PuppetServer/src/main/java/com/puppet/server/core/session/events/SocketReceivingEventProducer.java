package com.puppet.server.core.session.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lmax.disruptor.RingBuffer;

public class SocketReceivingEventProducer {

	private static final Logger logger = LoggerFactory.getLogger(SocketReceivingEventProducer.class);

	private final RingBuffer<SocketReceivingEvent> ringBuffer;

	public SocketReceivingEventProducer(RingBuffer<SocketReceivingEvent> ringBuffer) {
		this.ringBuffer = ringBuffer;
	}

	public void publish(long sessionId, byte[] data) {
		long sequence = ringBuffer.next(); // Grab the next sequence
		boolean okToPublish = false;
		try {
			SocketReceivingEvent event = ringBuffer.get(sequence);
			event.setSessionId(sessionId);
			event.setData(data);
			okToPublish = true;
		} catch (Exception e) {
			logger.debug("fucking error: ", e);
		} finally {
			if (okToPublish) {
				ringBuffer.publish(sequence);
			}
		}
	}
}
