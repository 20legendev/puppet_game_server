package com.puppet.server.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.puppet.server.core.session.PuSession;

final class PuSessionManagerImpl implements PuSessionManager {

	private static long sessionIdSeed = 0;

	private Map<Long, PuSession> idToSessionMap = new ConcurrentHashMap<Long, PuSession>();

	@Override
	public synchronized long registerNewSession(PuSession session) {
		long id = ++sessionIdSeed;
		this.idToSessionMap.put(id, session);
		return id;
	}

	@Override
	public PuSession deregisterSession(long id) {
		if (this.idToSessionMap.containsKey(id)) {
			return this.idToSessionMap.remove(id);
		}
		return null;
	}

	@Override
	public PuSession getSessionById(long id) {
		return this.idToSessionMap.get(id);
	}

}
