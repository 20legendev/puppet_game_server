package com.puppet.server.core.gateways;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import com.puppet.server.core.session.NettyNioTcpSession;

public class NettyNioBinaryTcpServer extends BaseNettyNioServer {

	// private static final Logger logger =
	// LoggerFactory.getLogger(NettyNioBinaryTcpServer.class);

	/**
	 * make this constructor internal, prevent other class outside this package
	 * from invoke this
	 */
	NettyNioBinaryTcpServer() {
	}

	private ChannelFuture channelFuture;
	private EventLoopGroup bossGroup;
	private EventLoopGroup workerGroup;
	private ServerBootstrap serverBootstrap;

	@Override
	public void start() throws Exception {
		bossGroup = new NioEventLoopGroup(); // (1)
		workerGroup = new NioEventLoopGroup();
		try {
			serverBootstrap = new ServerBootstrap(); // (2)
			serverBootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
					.childHandler(new ChannelInitializer<SocketChannel>() { // (4)
								@Override
								public void initChannel(SocketChannel ch) throws Exception {
									ch.pipeline().addLast(new NettyNioTcpSession());
								}
							});

			// Bind and start to accept incoming connections.
			channelFuture = serverBootstrap.bind(this.getGatewayConfig().getHost(), this.getGatewayConfig().getPort())
					.sync(); // (7)

			// Wait until the server socket is closed.
			// In this example, this does not happen, but you can do that to
			// gracefully
			// shut down your server.
			channelFuture.channel().closeFuture().sync();

			channelFuture = null;
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	@Override
	public void stop() {
		System.out.println("shutting down " + this.getGatewayConfig().getHost() + ":"
				+ this.getGatewayConfig().getPort());
	}

}
