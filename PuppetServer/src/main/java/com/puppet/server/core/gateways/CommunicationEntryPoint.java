package com.puppet.server.core.gateways;

import com.puppet.server.core.configuration.GatewayConfig;

public interface CommunicationEntryPoint {
	public void start() throws Exception;

	public void stop();

	public void setGatewayConfig(GatewayConfig config);

	public GatewayConfig getGatewayConfig();
}
