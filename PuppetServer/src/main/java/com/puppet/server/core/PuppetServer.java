package com.puppet.server.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.eventdriven.impl.EventDispatcherImpl;
import com.puppet.server.core.session.events.SocketReceivingController;

@SuppressWarnings("unused")
public class PuppetServer extends EventDispatcherImpl {

	private static PuppetServer _instance = null;

	private static final Logger logger = LoggerFactory.getLogger(PuppetServer.class);

	public static PuppetServer getInstance() {
		if (_instance == null) {
			_instance = new PuppetServer();
		}
		return _instance;
	}

	private PuppetServer() {
		// private constructor, this is singleton class
	}

	private PuUserManager userManager;
	private PuSessionManager sessionManager;

	public void start() throws Exception {
		this.userManager = new PuUserManagerImpl();
		this.sessionManager = new PuSessionManagerImpl();
		PuGatewayManager.getInstance().init(this.userManager, this.sessionManager);
	}

	public void stop() {
		SocketReceivingController.getInstance().shutdown();
	}

	public PuSessionManager getSessionManager() {
		return this.sessionManager;
	}
}
