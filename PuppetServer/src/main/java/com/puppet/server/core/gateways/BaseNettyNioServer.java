package com.puppet.server.core.gateways;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.server.core.PuSessionManager;
import com.puppet.server.core.PuUserManager;
import com.puppet.server.core.configuration.GatewayConfig;

public abstract class BaseNettyNioServer implements CommunicationEntryPoint {

	private static final Logger logger = LoggerFactory.getLogger(BaseNettyNioServer.class);

	private GatewayConfig gatewayConfig;
	private PuSessionManager sessionManager;
	private PuUserManager userManager;

	{
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				logger.info("shutting down server " + BaseNettyNioServer.this.getGatewayConfig().getName() + " at "
						+ BaseNettyNioServer.this.getGatewayConfig().getHost() + ":"
						+ BaseNettyNioServer.this.getGatewayConfig().getPort());
				BaseNettyNioServer.this.stop();
			}
		});
	}

	@Override
	public GatewayConfig getGatewayConfig() {
		return this.gatewayConfig;
	}

	@Override
	public void setGatewayConfig(GatewayConfig config) {
		this.gatewayConfig = config;
	}

	public PuSessionManager getSessionManager() {
		return sessionManager;
	}

	public void setSessionManager(PuSessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public PuUserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(PuUserManager userManager) {
		this.userManager = userManager;
	}

}
