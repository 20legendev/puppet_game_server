package com.puppet.server.core.gateways;

import com.puppet.server.statics.Protocol;

public final class CommunicationEntryPointFactory {

	private CommunicationEntryPointFactory() {
		// do nothing???
	}

	public static CommunicationEntryPoint newInstanceForProtocol(Protocol protocol) {
		switch (protocol) {
		case TCP:
			return new NettyNioBinaryTcpServer();
		default:
			return null;
		}
	}
}
