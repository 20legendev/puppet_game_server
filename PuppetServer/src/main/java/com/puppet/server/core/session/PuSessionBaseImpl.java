package com.puppet.server.core.session;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.puppet.eventdriven.Event;
import com.puppet.eventdriven.EventListener;

public abstract class PuSessionBaseImpl implements PuSession {

	private Map<String, List<EventListener>> listeners;

	@Override
	public void addEventListener(String eventType, EventListener listener) {
		if (this.listeners == null) {
			this.listeners = new ConcurrentHashMap<String, List<EventListener>>();
		}
		if (!this.listeners.containsKey(eventType)) {
			this.listeners.put(eventType, new CopyOnWriteArrayList<EventListener>());
		}
		this.listeners.get(eventType).add(listener);
	}

	@Override
	public void removeEventListener(String eventType, EventListener listener) {
		if (this.listeners != null && this.listeners.containsKey(eventType)) {
			if (listener == null) {
				this.listeners.remove(eventType);
			} else {
				this.listeners.get(eventType).remove(listener);
			}
		}
	}

	@Override
	public void removeAllEvent() {
		this.listeners = null;
	}

	@Override
	public void dispatchEvent(Event event) {
		String eventType = event.getType();
		if (eventType != null && this.listeners.containsKey(eventType)) {
			List<EventListener> tmpListeners = this.listeners.get(eventType);
			if (tmpListeners.size() > 0) {
				for (EventListener listener : tmpListeners) {
					try {
						listener.onEvent(event);
					} catch (Exception e) {
						throw new RuntimeException("error while event handled");
					}
				}
			}
		}
	}
}
