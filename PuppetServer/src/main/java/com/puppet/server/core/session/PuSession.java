package com.puppet.server.core.session;

import com.puppet.common.message.PuMessage;
import com.puppet.eventdriven.EventDispatcher;

public interface PuSession extends EventDispatcher {

	public long getId();
	
	public void sendMessage(byte[] message);

	public void sendMessage(PuMessage message);

}
