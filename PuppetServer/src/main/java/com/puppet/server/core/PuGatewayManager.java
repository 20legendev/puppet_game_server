package com.puppet.server.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.server.core.configuration.ServerConfiguration;
import com.puppet.server.core.gateways.BaseNettyNioServer;
import com.puppet.server.core.gateways.CommunicationEntryPoint;
import com.puppet.server.core.gateways.CommunicationEntryPointFactory;

@SuppressWarnings("unused")
final class PuGatewayManager {

	private static final Logger logger = LoggerFactory.getLogger(PuGatewayManager.class);

	private static PuGatewayManager _instance = null;

	static PuGatewayManager getInstance() {
		if (_instance == null) {
			_instance = new PuGatewayManager();
		}
		return _instance;
	}

	private PuUserManager userManager;
	private PuSessionManager sessionManager;

	private PuGatewayManager() {

	}

	public void init(PuUserManager userManager, PuSessionManager sessionManager) throws Exception {
		this.userManager = userManager;
		this.sessionManager = sessionManager;
		ServerConfiguration
				.getInstance()
				.getAllGateway()
				.parallelStream()
				.forEach(
						gatewayConfig -> {
							CommunicationEntryPoint communicationEntryPoint = CommunicationEntryPointFactory
									.newInstanceForProtocol(gatewayConfig.getProtocol());
							if (communicationEntryPoint != null) {
								logger.info("starting server for config : " + gatewayConfig);
								communicationEntryPoint.setGatewayConfig(gatewayConfig);
								if (communicationEntryPoint instanceof BaseNettyNioServer) {
									((BaseNettyNioServer) communicationEntryPoint).setSessionManager(sessionManager);
									((BaseNettyNioServer) communicationEntryPoint).setUserManager(userManager);
								}
								try {
									communicationEntryPoint.start();
								} catch (Exception e) {
									throw new RuntimeException(e);
								}
							} else {
								logger.warn("server is not available for config: " + gatewayConfig);
							}
						});
	}

	public PuSessionManager getSessionManager() {
		return this.sessionManager;
	}
}
