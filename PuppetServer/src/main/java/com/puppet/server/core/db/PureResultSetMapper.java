package com.puppet.server.core.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

public class PureResultSetMapper implements ResultSetMapper<ResultSet> {

	@Override
	public ResultSet map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		return r;
	}

}
