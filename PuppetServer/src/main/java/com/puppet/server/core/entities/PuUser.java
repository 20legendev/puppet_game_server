package com.puppet.server.core.entities;

import com.puppet.eventdriven.EventDispatcher;

public interface PuUser extends EventDispatcher {

	public String getUserName();

	public void setUserName(String userName);
}
