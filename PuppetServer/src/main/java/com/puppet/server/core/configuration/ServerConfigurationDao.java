package com.puppet.server.core.configuration;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.puppet.server.core.db.AbstractDerbyDao;

abstract class ServerConfigurationDao extends AbstractDerbyDao {

	@SqlQuery("SELECT * FROM \"puppet\".\"gateway\"")
	@RegisterMapper(GatewayConfigMapper.class)
	public abstract List<GatewayConfig> getAllGateway();

}
