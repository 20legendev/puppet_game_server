package com.puppet.server;

import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.server.core.PuppetServer;
import com.puppet.server.core.configuration.ServerConfiguration;
import com.puppet.server.utils.Initializer;

public class StartPuppetServer {

	static {
		Initializer.bootstrap(StartPuppetServer.class);
	}

	private static final Logger logger = LoggerFactory.getLogger(PuppetServer.class);

	public static void main(String[] args) throws Exception {
		try (InputStream is = StartPuppetServer.class.getResourceAsStream("/logo.txt")) {
			if (is != null) {
				StringWriter sw = new StringWriter();
				IOUtils.copy(is, sw);
				LoggerFactory.getLogger("pureLogger").info(sw.toString());
			} else {
				logger.info("*********** STARTING PUPPET SERVER ***********");
			}
		}

		ServerConfiguration.getInstance().init();
		PuppetServer.getInstance().start();
	}

}
