package com.puppet.server.statics;

import java.util.HashMap;
import java.util.Map;

public enum Protocol {

	TCP, UDP, HTTP, RMTP;

	private static Map<String, Protocol> map;

	public static Protocol fromName(String name) {
		if (name != null) {
			if (map == null) {
				map = new HashMap<String, Protocol>();
				for (Protocol tp : values()) {
					map.put(tp.name().toLowerCase(), tp);
				}
			}
			return map.get(name.toLowerCase());
		}
		return null;
	}
}
