package com.puppet.server.statics;

public abstract class Fields {
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String PORT = "port";
	public static final String PROTOCOL = "protocol";
	public static final String HOST = "host";
}
