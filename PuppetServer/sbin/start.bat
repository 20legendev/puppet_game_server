@ECHO off
if NOT DEFINED PUPPET_HOME set PUPPET_HOME=%~dp0

set JAVA_OPTS=^
 -ea^
 -Xms1G^
 -Xmx1G^
 -XX:+HeapDumpOnOutOfMemoryError^
 -XX:+UseParNewGC^
 -XX:+UseConcMarkSweepGC^
 -XX:+CMSParallelRemarkEnabled^
 -XX:SurvivorRatio=8^
 -XX:MaxTenuringThreshold=1^
 -XX:CMSInitiatingOccupancyFraction=75^
 -XX:+UseCMSInitiatingOccupancyOnly

set CLASSPATH="%PUPPET_HOME%lib\*"
set MAIN_CLASS=com.puppet.server.Main

"%JAVA_HOME%\bin\java" %JAVA_OPTS% -cp %CLASSPATH% %MAIN_CLASS% %*

REM pause
