package com.puppet.client.test.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import com.puppet.client.test.events.*;


public class ConnectForm extends Composite {
	private Text hostInput;
	private Text portInput;

	public ConnectForm(Composite parent, int style) {
		super(parent, style);
		setLayout(new FormLayout());

		Group group = new Group(this, SWT.NONE);
		group.setText("Connect to server");

		FormData fd_group = new FormData();
		fd_group.bottom = new FormAttachment(100);
		fd_group.right = new FormAttachment(100);
		fd_group.top = new FormAttachment(0);
		fd_group.left = new FormAttachment(0);
		group.setLayoutData(fd_group);

		hostInput = new Text(group, SWT.BORDER);
		FormData fd_hostInput = new FormData();
		fd_hostInput.right = new FormAttachment(100, -10);
		fd_hostInput.top = new FormAttachment(0, 10);
		fd_hostInput.left = new FormAttachment(0, 10);
		hostInput.setLayoutData(fd_hostInput);
		hostInput.setBounds(10, 22, 239, 24);
		hostInput.setText("127.0.0.1");

		portInput = new Text(group, SWT.BORDER);
		FormData fd_portInput = new FormData();
		fd_portInput.right = new FormAttachment(hostInput, 0, SWT.RIGHT);
		fd_portInput.top = new FormAttachment(hostInput, 6);
		fd_portInput.left = new FormAttachment(hostInput, 0, SWT.LEFT);
		portInput.setLayoutData(fd_portInput);
		portInput.setBounds(10, 52, 239, 24);
		portInput.setText("9981");

		Button connectBtn = new Button(group, SWT.NONE);
		FormData fd_connectBtn = new FormData();
		fd_connectBtn.right = new FormAttachment(hostInput, 0, SWT.RIGHT);
		fd_connectBtn.top = new FormAttachment(portInput, 6);
		fd_connectBtn.left = new FormAttachment(hostInput, 0, SWT.LEFT);
		connectBtn.setLayoutData(fd_connectBtn);
		connectBtn.setBounds(10, 105, 239, 23);
		connectBtn.setText("Connect");

		Button btnUseSsl = new Button(group, SWT.CHECK);
		btnUseSsl.setBounds(10, 82, 85, 16);
		btnUseSsl.setText("use ssl");

		connectBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				CenterObserver.getInstance().notifyCommand("executeConnect", "host", hostInput.getText(), "port",
						Integer.valueOf(portInput.getText()), "ssl", btnUseSsl.getSelection());
			}
		});
	}
}
