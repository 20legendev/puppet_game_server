package com.puppet.client.test.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.client.PuppetClient;
import com.puppet.client.test.events.CenterObserver;
import com.puppet.common.PuEventType;
import com.puppet.common.PuField;
import com.puppet.common.message.concrete.PingMessage;
import com.puppet.eventdriven.Event;
import com.puppet.eventdriven.EventListener;
import com.puppet.eventdriven.impl.BaseEvent;

public class MainApplication {

	private static final Logger logger = LoggerFactory.getLogger(MainApplication.class);

	private static MainApplication instance = new MainApplication();

	public static MainApplication getInstance() {
		return instance;
	}

	private PuppetClient puppetClient = null;

	protected Shell shell;
	private FormToolkit formToolkit = null;
	private WorkingForm workingForm = null;
	private ConnectForm connectForm = null;

	private MainApplication() {
	}

	private EventListener onConnected = new EventListener() {

		@Override
		public void onEvent(Event event) {
			BaseEvent baseEvent = (BaseEvent) event;
			boolean successful = (boolean) baseEvent.get(PuField.SUCCESSFUL);
			logger.debug("connection response: " + event);
			updateGui(new Runnable() {
				public void run() {
					if (successful) {
						logger.debug("connected...");
						connectForm.dispose();
						initWorkingForm();
						shell.layout(true, true);
					} else {
						MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
						messageBox.setText("Error");
						Exception ex = (Exception) baseEvent.get(PuField.CAUSE);
						if (ex != null) {
							messageBox.setMessage(ex.getMessage() == null ? "No message" : ex.getMessage());
							ex.printStackTrace();
						} else {
							messageBox.setMessage("Unknown reason");
						}
						messageBox.open();
					}
				}
			});
		}
	};

	private EventListener onDisconnected = new EventListener() {

		@Override
		public void onEvent(Event event) throws Exception {
			updateGui(new Runnable() {
				public void run() {
					logger.debug("disconnected...");
					workingForm.dispose();
					initConnectForm();
					shell.layout(true, true);
				}
			});
		}
	};

	private EventListener onPingPong = new EventListener() {

		@Override
		public void onEvent(Event event) throws Exception {
			BaseEvent baseEvent = (BaseEvent) event;
			updateGui(new Runnable() {
				public void run() {
					long time = (long) baseEvent.get("delay");
					workingForm.updatePingAvg(time);
				}
			});
		}
	};

	/**
	 * Open the window.
	 */
	public void open() {
		logger.debug("openning window...");
		formToolkit = new FormToolkit(Display.getDefault());
		createContents();
		shell.open();
		shell.layout();

		this.listenAll();

		Display display = Display.getDefault();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
		logger.debug("shell disposed...");
		if (puppetClient != null) {
			logger.debug("trying to destroy puppet client...");
			puppetClient.destroy();
			logger.debug("connection destroyed");
		}
	}

	private void listenAll() {
		CenterObserver.getInstance().registerCommand("executeConnect", new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				BaseEvent baseEvent = (BaseEvent) event;
				String host = (String) baseEvent.get("host");
				int port = (int) baseEvent.get("port");
				boolean ssl = (boolean) baseEvent.get("ssl");

				logger.debug("connecting to " + host + ":" + port + (ssl ? " using ssl" : "") + "...");

				if (puppetClient == null) {
					puppetClient = new PuppetClient();
					puppetClient.addEventListener(PuEventType.CONNECTION_RESPONSE, onConnected);
					puppetClient.addEventListener(PuEventType.DISCONNECTED, onDisconnected);
					puppetClient.addEventListener(PuEventType.PING_PONG, onPingPong);
				} else {
					puppetClient.disconnect();
				}
				puppetClient.connect(host, port, ssl);
			}
		});

		CenterObserver.getInstance().registerCommand("executeDisconnect", new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				if (puppetClient != null) {
					puppetClient.disconnect();
				}
			}
		});

		CenterObserver.getInstance().registerCommand("ping", new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				if (puppetClient != null) {
					logger.debug("sending ping message...");
					PingMessage pingMessage = new PingMessage();
					puppetClient.send(pingMessage);
				}
			}
		});

		CenterObserver.getInstance().registerCommand("sendMessage", new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				BaseEvent baseEvent = (BaseEvent) event;
				String text = (String) baseEvent.get("text");
				System.out.println("trying to send data: `" + text + "`, string length: " + text.length()
						+ ", bytes length: " + text.getBytes().length);
			}
		});

		CenterObserver.getInstance().registerCommand("enableLagMonitor", new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				puppetClient.enableLagMonitor((long) ((BaseEvent) event).get("interval"));
			}
		});

		CenterObserver.getInstance().registerCommand("disableLagMonitor", new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				puppetClient.enableLagMonitor(-1);
			}
		});
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		logger.debug("creating contents");
		shell = new Shell();
		shell.setSize(718, 538);
		shell.setText("SWT Application");
		shell.setLayout(new FormLayout());

		initConnectForm();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initConnectForm() {
		connectForm = new ConnectForm(shell, SWT.NONE);
		FormData fd_connectForm = new FormData();
		fd_connectForm.top = new FormAttachment(0, 165);
		fd_connectForm.right = new FormAttachment(100, -224);
		fd_connectForm.bottom = new FormAttachment(0, 301);
		fd_connectForm.left = new FormAttachment(0, 226);
		connectForm.setLayoutData(fd_connectForm);
		formToolkit.adapt(connectForm);
		formToolkit.paintBordersFor(connectForm);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initWorkingForm() {
		workingForm = new WorkingForm(shell, SWT.NONE);
		FormData fd_workingForm = new FormData();
		fd_workingForm.bottom = new FormAttachment(100);
		fd_workingForm.top = new FormAttachment(0);
		fd_workingForm.left = new FormAttachment(0);
		fd_workingForm.right = new FormAttachment(100);
		workingForm.setLayoutData(fd_workingForm);
		formToolkit.adapt(workingForm);
		formToolkit.paintBordersFor(workingForm);
	}

	private void updateGui(Runnable runnable) {
		Display.getDefault().syncExec(runnable);
	}
}
