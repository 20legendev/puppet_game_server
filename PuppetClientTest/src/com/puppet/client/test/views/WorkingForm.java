package com.puppet.client.test.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.client.test.events.CenterObserver;

public class WorkingForm extends Composite {

	private static final Logger logger = LoggerFactory.getLogger(WorkingForm.class);

	private Text text;
	private Text pingIntervalInput;
	private Label pingAvgLabel = null;
	private String lastPingIntervalValue = null;

	// private static final Logger logger =
	// LoggerFactory.getLogger(WorkingForm.class);

	public WorkingForm(Composite parent, int style) {
		super(parent, style);
		setLayout(new FormLayout());

		Button btnDisconnect = new Button(this, SWT.NONE);
		btnDisconnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				CenterObserver.getInstance().notifyCommand("executeDisconnect");
			}
		});

		FormData fd_btnDisconnect = new FormData();
		fd_btnDisconnect.right = new FormAttachment(100, -13);
		btnDisconnect.setLayoutData(fd_btnDisconnect);
		btnDisconnect.setText("Disconnect");

		Button sendBtn = new Button(this, SWT.NONE);
		sendBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				CenterObserver.getInstance().notifyCommand("sendMessage", "text", text.getText());
			}
		});
		FormData fd_sendBtn = new FormData();
		fd_sendBtn.right = new FormAttachment(100, -13);
		fd_sendBtn.bottom = new FormAttachment(100, -10);
		sendBtn.setLayoutData(fd_sendBtn);
		sendBtn.setText("Send");

		text = new Text(this, SWT.NONE);
		text.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == 13) {
					CenterObserver.getInstance().notifyCommand("sendMessage", "text", text.getText());
					text.setText("");
				}
			}
		});

		FormData fd_text = new FormData();
		fd_text.left = new FormAttachment(0, 10);
		fd_text.right = new FormAttachment(sendBtn, -6);
		fd_text.bottom = new FormAttachment(sendBtn, 0, SWT.BOTTOM);
		fd_text.top = new FormAttachment(sendBtn, 0, SWT.TOP);
		text.setLayoutData(fd_text);

		StyledText styledText = new StyledText(this, SWT.BORDER | SWT.READ_ONLY);
		fd_btnDisconnect.bottom = new FormAttachment(styledText, -6);
		styledText.setIndent(3);
		FormData fd_styledText = new FormData();
		fd_styledText.bottom = new FormAttachment(sendBtn, -6);
		fd_styledText.top = new FormAttachment(0, 39);
		fd_styledText.left = new FormAttachment(text, 0, SWT.LEFT);
		fd_styledText.right = new FormAttachment(100, -13);
		styledText.setLayoutData(fd_styledText);

		Button btnPing = new Button(this, SWT.NONE);
		btnPing.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				CenterObserver.getInstance().notifyCommand("ping");
			}
		});
		FormData fd_btnPing = new FormData();
		fd_btnPing.top = new FormAttachment(btnDisconnect, 0, SWT.TOP);
		fd_btnPing.left = new FormAttachment(text, 0, SWT.LEFT);
		btnPing.setLayoutData(fd_btnPing);
		btnPing.setText("Ping");

		Button btnAutoPing = new Button(this, SWT.CHECK);
		btnAutoPing.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		btnAutoPing.setVisible(true);
		FormData fd_btnAutoPing = new FormData();
		fd_btnAutoPing.top = new FormAttachment(btnDisconnect, 4, SWT.TOP);
		fd_btnAutoPing.left = new FormAttachment(btnPing, 6);
		btnAutoPing.setLayoutData(fd_btnAutoPing);
		btnAutoPing.setText("Auto ping");

		Composite composite = new Composite(this, SWT.NONE);
		composite.setEnabled(false);
		composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		composite.setVisible(true);
		FormData fd_composite = new FormData();
		fd_composite.bottom = new FormAttachment(btnDisconnect, 0, SWT.BOTTOM);
		fd_composite.left = new FormAttachment(btnAutoPing, 5);
		fd_composite.top = new FormAttachment(0, 10);
		composite.setLayoutData(fd_composite);

		Label lblIntervalTime = new Label(composite, SWT.NONE);
		lblIntervalTime.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblIntervalTime.setBounds(0, 5, 83, 13);
		lblIntervalTime.setText("Interval time (s)");

		pingIntervalInput = new Text(composite, SWT.BORDER);
		pingIntervalInput.setText("1");
		pingIntervalInput.setBounds(83, 2, 62, 19);
		lastPingIntervalValue = pingIntervalInput.getText();

		pingIntervalInput.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == 13) {
					float value = -1;
					try {
						value = Float.valueOf(pingIntervalInput.getText().trim());
					} catch (Exception ex) {
						logger.debug("error while trying to parse ping interval value", e);
						pingIntervalInput.setText(lastPingIntervalValue);
						return;
					}
					lastPingIntervalValue = String.valueOf(value);
					if (btnAutoPing.getSelection()) {
						CenterObserver.getInstance().notifyCommand("enableLagMonitor", "interval",
								(long) Math.round(value * 1000));
					}
				}
			}
		});

		Label lblPingAvg = new Label(this, SWT.NONE);
		lblPingAvg.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		fd_composite.right = new FormAttachment(lblPingAvg, -6);
		FormData fd_lblPingAvg = new FormData();
		fd_lblPingAvg.left = new FormAttachment(0, 275);
		fd_lblPingAvg.top = new FormAttachment(btnDisconnect, 5, SWT.TOP);
		lblPingAvg.setLayoutData(fd_lblPingAvg);
		lblPingAvg.setText("Ping avg:");

		pingAvgLabel = new Label(this, SWT.NONE);
		pingAvgLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		pingAvgLabel.setText("0");
		FormData fd_pingAvgLabel = new FormData();
		fd_pingAvgLabel.right = new FormAttachment(lblPingAvg, 70, SWT.RIGHT);
		fd_pingAvgLabel.top = new FormAttachment(btnDisconnect, 5, SWT.TOP);
		fd_pingAvgLabel.left = new FormAttachment(lblPingAvg, 6);
		pingAvgLabel.setLayoutData(fd_pingAvgLabel);

		btnAutoPing.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				composite.setEnabled(btnAutoPing.getSelection());
				if (btnAutoPing.getSelection()) {
					CenterObserver.getInstance().notifyCommand("enableLagMonitor", "interval",
							(long) Math.round(Float.valueOf(pingIntervalInput.getText()) * 1000));
				} else {
					CenterObserver.getInstance().notifyCommand("disableLagMonitor");
				}
			}
		});
	}

	public void updatePingAvg(long timeNano) {
		this.pingAvgLabel.setText(String.format("%.2f ms", timeNano / 1e6));
	}
}
