package com.puppet.client.test;

import java.util.concurrent.CountDownLatch;

import com.puppet.client.utils.Initializer;

import com.puppet.client.test.views.*;

public class PuppetClientTest {
	static {
		Initializer.bootstrap(PuppetClientTest.class);
	}

	private static CountDownLatch endSignal = new CountDownLatch(1);

	public static void main(String[] args) {
		(new Thread() {
			{
				setName("GUI-Thread");
			}

			@Override
			public void run() {
				MainApplication.getInstance().open();
				endSignal.countDown();
			}
		}).start();

		try {
			endSignal.await();
			System.exit(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.exit(1);
	}
}
