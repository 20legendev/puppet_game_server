package com.puppet.client.test.events;

import com.puppet.eventdriven.observer.Observer;

public class CenterObserver extends Observer {
	private static final CenterObserver instance = new CenterObserver();

	public static final CenterObserver getInstance() {
		return instance;
	}

	private CenterObserver() {
	}
}
