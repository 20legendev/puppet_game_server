package com.puppet.common;

import java.nio.ByteBuffer;

public enum PuCommand {
	ERROR,
	PING,
	PONG,
	LOGIN,
	LOGOUT,
	JOIN_ROOM,
	JOIN_ROOM_RESPONSE,
	LEAVE_ROOM,
	LEAVE_ROOM_RESPONSE,
	KICKED_FROM_ROOM,
	KICKED_FROM_SERVER,
	BUDDY_REQUEST,
	BUDDY_RESPONSE,
	USER_JOIN_ROOM,
	USER_LEAVE_ROOM;

	private static int idSeed = 0;

	private static int getNextId() {
		return idSeed++;
	}

	private int id;
	private byte[] bytes;

	private PuCommand() {
		this.id = getNextId();
	}

	public int getId() {
		return this.id;
	}

	public String toString() {
		return this.name() + "(" + this.id + ")";
	}

	public byte[] getBytes() {
		if (this.bytes == null) {
			if (id < -(1 << 5)) {
				if (id < -(1 << 15)) {
					// signed 32
					this.bytes = ByteBuffer.allocate(4).putInt(id).array();
				} else if (id < -(1 << 7)) {
					// signed 16
					this.bytes = ByteBuffer.allocate(2).putShort((short) id).array();
				} else {
					// signed 8
					this.bytes = ByteBuffer.allocate(1).put((byte) id).array();
				}
			} else if (id < (1 << 7)) {
				// fixnum
				this.bytes = ByteBuffer.allocate(1).put((byte) id).array();
			} else {
				if (id < (1 << 8)) {
					// unsigned 8
					this.bytes = ByteBuffer.allocate(1).put((byte) id).array();
				} else if (id < (1 << 16)) {
					// unsigned 16
					this.bytes = ByteBuffer.allocate(2).putShort((short) id).array();
				} else {
					// unsigned 32
					this.bytes = ByteBuffer.allocate(4).putInt(id).array();
				}
			}
		}
		return this.bytes;
	}

	public static PuCommand fromId(int id) {
		for (PuCommand cmd : values()) {
			if (cmd.id == id) {
				return cmd;
			}
		}
		return null;
	}

	public static PuCommand fromId(byte[] id) {
		ByteBuffer cmdByteBuffer = ByteBuffer.wrap(id);
		switch (cmdByteBuffer.capacity()) {
		case 1:
			return PuCommand.fromId(cmdByteBuffer.get());
		case 2:
			return PuCommand.fromId(cmdByteBuffer.getShort());
		case 4:
			return PuCommand.fromId(cmdByteBuffer.getInt());
		}
		return null;
	}
}
