package com.puppet.common;

public class PuEventType {
	public static final String PING = "ping";
	public static final String PONG = "pong";
	public static final String LOGIN_RESPONSE = "loginResponse";
	public static final String LOGGED_OUT = "loggedOut";
	public static final String DISCONNECTED = "disconnected";
	public static final String CONNECTION_RESPONSE = "connectionResponse";
	public static final String PING_PONG = "pingPong";
}
