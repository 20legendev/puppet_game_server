package com.puppet.common;

public class PuField {
	public static final String COMMAND = "c";
	public static final String DATA = "d";
	public static final String ZONE = "z";
	public static final String ZONE_ID = "zi";
	public static final String ZONE_NAME = "zn";
	public static final String ROOM = "r";
	public static final String ROOM_ID = "ri";
	public static final String ROOM_NAME = "rn";
	public static final String ROOM_GROUP = "rg";
	public static final String SUCCESSFUL = "sc";
	public static final String CAUSE = "cs";
}
