package com.puppet.common.message;

import com.puppet.common.PuCommand;

public abstract class PuMessageBase implements PuMessage {

	private PuCommand command;
	private byte[] data;

	public PuCommand getCommand() {
		return command;
	}

	public void setCommand(PuCommand command) {
		this.command = command;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
}
