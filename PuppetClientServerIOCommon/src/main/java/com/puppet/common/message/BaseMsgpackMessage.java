package com.puppet.common.message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.msgpack.template.Templates;

import com.puppet.common.PuField;

public abstract class BaseMsgpackMessage extends PuMessageBase {

	private static final MessagePack msgpack = new MessagePack();
	private static final Template<Map<String, byte[]>> baseTemplate = Templates.tMap(Templates.TString,
			Templates.TByteArray);

	private final Map<String, byte[]> toMap() throws IOException {
		if (this.getCommand() != null) {
			Map<String, byte[]> result = new HashMap<String, byte[]>();
			result.put(PuField.COMMAND, this.getCommand().getBytes());
			if (this.getData() != null) {
				result.put(PuField.DATA, this.getData());
			}
			this.buildMap(result);
			return result;
		}
		return null;
	}

	protected void buildMap(Map<String, byte[]> map) {

	}

	@Override
	public final byte[] getBytes() throws IOException {
		Map<String, byte[]> map = this.toMap();
		if (map != null) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			baseTemplate.write(msgpack.createPacker(out), map);
			return out.toByteArray();
		}
		return null;
	}
}
