package com.puppet.common.message.concrete;

import com.puppet.common.PuCommand;
import com.puppet.common.message.BaseMsgpackMessage;

public class PongMessage extends BaseMsgpackMessage {

	public PongMessage(byte[] data) {
		this.setCommand(PuCommand.PONG);
		this.setData(data);
	}
}
