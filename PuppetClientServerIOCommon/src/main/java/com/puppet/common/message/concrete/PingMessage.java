package com.puppet.common.message.concrete;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import com.puppet.common.PuCommand;
import com.puppet.common.message.BaseMsgpackMessage;

public class PingMessage extends BaseMsgpackMessage {

	private static final Map<String, Long> messageIdToTimeNano = new ConcurrentHashMap<String, Long>();

	public static final Long getTimeForMessageId(byte[] messageId) {
		if (messageId == null) {
			return null;
		}
		return messageIdToTimeNano.get(new String(messageId));
	}
	
	public static final Long removeTimeForMessageId(byte[] messageId) {
		if (messageId == null) {
			return null;
		}
		return messageIdToTimeNano.remove(new String(messageId));
	}

	private static final Random rand = new Random();

	public PingMessage() {
		this.setCommand(PuCommand.PING);
		byte[] bytes = new byte[4];
		rand.nextBytes(bytes);
		this.setData(bytes);
	}

	public void logSentTime() {
		messageIdToTimeNano.put(new String(this.getData()), System.nanoTime());
	}
}
