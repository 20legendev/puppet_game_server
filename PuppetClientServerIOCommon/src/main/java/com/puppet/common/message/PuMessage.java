package com.puppet.common.message;

import java.io.IOException;

import com.puppet.common.PuCommand;

public interface PuMessage {
	public PuCommand getCommand();

	public byte[] getBytes() throws IOException;
}
