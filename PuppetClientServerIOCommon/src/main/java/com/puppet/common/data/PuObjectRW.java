package com.puppet.common.data;

import java.util.Collection;

public interface PuObjectRW extends PuObjectRO {
	// generic
	public void fromXML(String xml);

	public void set(String fieldName, Object value, PuDataType type);

	public Object remove(String fieldName);

	public void addAll(PuObjectRO source);

	// boolean
	public void setBoolean(String fieldName, Boolean value);

	public void setBooleanArray(String fieldName, Collection<Boolean> value);

	// byte
	public void setByte(String fieldName, Byte value);

	public void setByteArray(String fieldName, Collection<Byte> value);

	// short
	public void setShort(String fieldName, Short value);

	public void setShortArray(String fieldName, Collection<Short> value);

	// int
	public void setInteger(String fieldName, Integer value);

	public void setIntegerArray(String fieldName, Collection<Integer> value);

	// long
	public void setLong(String fieldName, Long value);

	public void setLongArray(String fieldName, Collection<Long> value);
	
	// float
	public void setFloat(String fieldName, Float value);

	public void setFloatArray(String fieldName, Collection<Float> value);

	// double
	public void setDouble(String fieldName, Double value);

	public void setDoubleArray(String fieldName, Collection<Double> value);

	// string
	public void setString(String fieldName, String value);

	public void setStringArray(String fieldName, Collection<String> value);

	// pu object
	public void setPuObject(String fieldName, PuObject value);

	public void setPuObjectArray(String fieldName, Collection<PuObject> value);
}