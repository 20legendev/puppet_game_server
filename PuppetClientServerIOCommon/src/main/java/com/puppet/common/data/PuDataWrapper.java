package com.puppet.common.data;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class PuDataWrapper {
	private Object data;
	private PuDataType type;

	public PuDataWrapper(Object data, PuDataType type) {
		if (type != null && type != PuDataType.NULL && type.isArray() && !(data instanceof Collection<?>)) {
			throw new IllegalArgumentException("Invalid data for type " + type + ", expecting for collection of "
					+ type.getDataType().getSimpleName());
		}
		if (data == null && type != PuDataType.NULL) {
			throw new IllegalArgumentException("cannot wrap null data");
		}
		this.data = data;
		this.type = type;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public PuDataType getType() {
		return type;
	}

	public void setType(PuDataType type) {
		this.type = type;
	}

	public List<Object> toTuple() {
		return Arrays.asList(this.type.getTypeId(), this.data);
	}
}