package com.puppet.common.exceptions;

public class XmlParseError extends Exception {

	public XmlParseError(String string) {
		super(string);
	}

	public XmlParseError() {
		// do nothing;
	}

	private static final long serialVersionUID = -1271004379849629344L;

}