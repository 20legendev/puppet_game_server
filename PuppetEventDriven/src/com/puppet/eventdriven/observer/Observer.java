package com.puppet.eventdriven.observer;

import com.puppet.eventdriven.Event;
import com.puppet.eventdriven.EventListener;
import com.puppet.eventdriven.impl.BaseEvent;
import com.puppet.eventdriven.impl.EventDispatcherImpl;

public class Observer extends EventDispatcherImpl {

	public void notifyCommand(String command, Object... data) {
		super.dispatchEvent(new BaseEvent(command, data));
	}

	public void registerCommand(String command, EventListener listener) {
		super.addEventListener(command, listener);
	}

	@Override
	@Deprecated
	public void addEventListener(String eventType, EventListener listener) {
		throw new UnsupportedOperationException("user registerCommand method instead");
	}

	@Override
	@Deprecated
	public void dispatchEvent(Event event) {
		throw new UnsupportedOperationException("user notifyCommand method instead");
	}

	@Override
	@Deprecated
	public void dispatchEvent(String eventType) {
		throw new UnsupportedOperationException("user notifyCommand method instead");
	}
}
