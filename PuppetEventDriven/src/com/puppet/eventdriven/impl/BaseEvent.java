package com.puppet.eventdriven.impl;

import java.util.HashMap;

import com.puppet.eventdriven.Event;
import com.puppet.eventdriven.EventCallback;
import com.puppet.eventdriven.EventDispatcher;

public class BaseEvent extends HashMap<String, Object> implements Event {

	private static final long serialVersionUID = -2206533823729431514L;
	private String type;
	private EventCallback callback;
	private EventDispatcher target;

	public BaseEvent() {
		
	}
	
	public BaseEvent(String type) {
		this.setType(type);
	}

	public BaseEvent(String type, Object... dataKeyValues) {
		this(type);
		for (int i = 0; i < dataKeyValues.length; i += 2) {
			this.put(dataKeyValues[i].toString(), dataKeyValues[i + 1]);
		}
	}

	public BaseEvent(String type, EventCallback callback) {
		this(type);
		this.setCallback(callback);
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public void setCallback(EventCallback callback) {
		this.callback = callback;
	}

	@Override
	public EventCallback getCallback() {
		return this.callback;
	}

	@Override
	public EventDispatcher getTarget() {
		return this.target;
	}

	@Override
	public void setTarget(EventDispatcher target) {
		this.target = target;
	}

}
