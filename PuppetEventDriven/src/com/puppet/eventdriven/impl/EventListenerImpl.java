package com.puppet.eventdriven.impl;

import java.lang.reflect.Method;

import com.puppet.eventdriven.Event;
import com.puppet.eventdriven.EventListener;

public class EventListenerImpl implements EventListener {

	private Object handler;
	private String methodName;
	private Method targetMethod;

	public EventListenerImpl() {
		// do nothing
	}

	public EventListenerImpl(Object handler, String methodName) {
		this.handler = handler;
		this.methodName = methodName;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (targetMethod == null) {
			targetMethod = this.handler.getClass().getMethod(methodName, Event.class);
		}
		targetMethod.invoke(handler, event);
	}

}
