package com.puppet.eventdriven;

public interface Event {

	public void setType(String type);

	public String getType();

	public void setCallback(EventCallback callback);

	public EventCallback getCallback();

	public EventDispatcher getTarget();

	public void setTarget(EventDispatcher target);
}
