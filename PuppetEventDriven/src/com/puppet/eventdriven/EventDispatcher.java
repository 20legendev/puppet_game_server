package com.puppet.eventdriven;

public interface EventDispatcher {
	public void addEventListener(String eventType, EventListener listener);

	public void removeEventListener(String eventType, EventListener listener);

	public void removeAllEvent();

	public void dispatchEvent(Event event);

	public void dispatchEvent(String eventType);
}
