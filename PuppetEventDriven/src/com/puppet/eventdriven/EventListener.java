package com.puppet.eventdriven;

public interface EventListener {
	public void onEvent(Event event) throws Exception;
}
