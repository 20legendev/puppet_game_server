package com.puppet.eventdriven;

public interface EventCallback {
	public void callback(Object data);
}
