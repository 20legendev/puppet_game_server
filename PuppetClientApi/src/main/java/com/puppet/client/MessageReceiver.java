package com.puppet.client;

import static org.msgpack.template.Templates.TByteArray;
import static org.msgpack.template.Templates.TString;
import static org.msgpack.template.Templates.tMap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.io.ByteArrayInputStream;
import java.util.Map;

import org.msgpack.MessagePack;
import org.msgpack.template.Template;
import org.msgpack.unpacker.Unpacker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppet.common.PuCommand;
import com.puppet.common.PuEventType;
import com.puppet.common.PuField;
import com.puppet.common.message.concrete.PingMessage;
import com.puppet.eventdriven.impl.BaseEvent;

class MessageReceiver extends ChannelInboundHandlerAdapter {

	private static final Logger logger = LoggerFactory.getLogger(MessageReceiver.class);

	private static final MessagePack msgpack = new MessagePack();

	private PuppetClient client;

	public MessageReceiver(PuppetClient puppetClient) {
		this.setClient(puppetClient);
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

		ByteBuf bb = (ByteBuf) msg;
		byte[] bytes = new byte[bb.readableBytes()];
		bb.readBytes(bytes);

		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		Unpacker unpacker = msgpack.createUnpacker(in);

		Template<Map<String, byte[]>> mapTmpl = tMap(TString, TByteArray);
		Map<String, byte[]> dstMap = unpacker.read(mapTmpl);

		PuCommand command = PuCommand.fromId(dstMap.get(PuField.COMMAND));

		switch (command) {
		case PONG:
			byte[] data = dstMap.get(PuField.DATA);
			this.client.dispatchEvent(new BaseEvent(PuEventType.PONG, "data", data));
			Long startTime = PingMessage.getTimeForMessageId(data);
			PingMessage.removeTimeForMessageId(data);
			if (startTime != null) {
				this.client.dispatchEvent(new BaseEvent(PuEventType.PING_PONG, "delay", System.nanoTime() - startTime));
			} else {
				logger.warn("got pong message, but message id not found");
			}
			break;
		default:
			logger.debug("unrecognized command: " + command);
			break;
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		logger.error("error: ", cause);
		// ctx.close();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		this.client.dispatchEvent(new BaseEvent(PuEventType.CONNECTION_RESPONSE, PuField.SUCCESSFUL, true));
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		this.client.dispatchEvent(new BaseEvent(PuEventType.DISCONNECTED));
	}

	public PuppetClient getClient() {
		return client;
	}

	public void setClient(PuppetClient client) {
		this.client = client;
	}
}
