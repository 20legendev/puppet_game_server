package com.puppet.client;

import io.netty.channel.ChannelPromise;

public interface SocketSender {
	public void send(byte[] bytes);

	public ChannelPromise sendPromise(byte[] bytes);
}
