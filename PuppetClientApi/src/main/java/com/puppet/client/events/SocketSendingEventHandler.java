package com.puppet.client.events;

import io.netty.channel.ChannelPromise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lmax.disruptor.WorkHandler;

public class SocketSendingEventHandler implements WorkHandler<SocketSendingEvent> {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SocketSendingEventHandler.class);

	private ChannelPromise lastChannelPromise;

	@Override
	public synchronized void onEvent(SocketSendingEvent event) throws Exception {
		// logger.debug("sending " + event.getData().length + " bytes data" + (event.isPromised() ? " with promised" : ""));
		if (event.isPromised()) {
			lastChannelPromise = event.getSender().sendPromise(event.getData());
			try {
				if (lastChannelPromise != null && !lastChannelPromise.isDone()) {
					lastChannelPromise.await();
				}
			} finally {
				lastChannelPromise = null;
			}
		} else {
			event.getSender().send(event.getData());
		}

		if (event.getCallback() != null) {
			event.getCallback().callback(null);
		}
	}
}
