package com.puppet.client.events;

import com.lmax.disruptor.EventFactory;
import com.puppet.client.SocketSender;

public class SocketSendingEventFactory implements EventFactory<SocketSendingEvent> {

	private SocketSender sender;

	public SocketSendingEventFactory(SocketSender sender) {
		this.sender = sender;
	}

	@Override
	public SocketSendingEvent newInstance() {
		return new SocketSendingEvent(this.sender);
	}

}
