package com.puppet.client.events;

import com.lmax.disruptor.RingBuffer;
import com.puppet.eventdriven.EventCallback;

public class SocketSendingEventProducer {
	// private static final Logger logger =
	// LoggerFactory.getLogger(SocketSendingEventProducer.class);

	private final RingBuffer<SocketSendingEvent> ringBuffer;

	public SocketSendingEventProducer(RingBuffer<SocketSendingEvent> ringBuffer) {
		this.ringBuffer = ringBuffer;
	}

	public void publish(byte[] data) {
		long sequence = ringBuffer.next();
		try {
			SocketSendingEvent event = ringBuffer.get(sequence);
			event.setData(data);
			event.setPromised(true);
		} finally {
			ringBuffer.publish(sequence);
		}
	}

	public void publish(byte[] data, EventCallback callback) {
		long sequence = ringBuffer.next();
		try {
			SocketSendingEvent event = ringBuffer.get(sequence);
			event.setData(data);
			event.setCallback(callback);
			event.setPromised(false);
		} finally {
			ringBuffer.publish(sequence);
		}
	}

	public void publish(byte[] data, EventCallback callback, boolean isPromised) {
		long sequence = ringBuffer.next();
		try {
			SocketSendingEvent event = ringBuffer.get(sequence);
			event.setData(data);
			event.setCallback(callback);
			event.setPromised(isPromised);
		} finally {
			ringBuffer.publish(sequence);
		}
	}
}
