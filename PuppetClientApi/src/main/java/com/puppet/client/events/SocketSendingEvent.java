package com.puppet.client.events;

import com.puppet.client.SocketSender;
import com.puppet.eventdriven.EventCallback;

public class SocketSendingEvent {
	private byte[] data;
	private SocketSender sender;
	private EventCallback callback;
	private boolean promised = false;

	public SocketSendingEvent(SocketSender sender) {
		this.setSender(sender);
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public SocketSender getSender() {
		return sender;
	}

	public void setSender(SocketSender sender) {
		this.sender = sender;
	}

	public EventCallback getCallback() {
		return callback;
	}

	public void setCallback(EventCallback callback) {
		this.callback = callback;
	}

	public boolean isPromised() {
		return promised;
	}

	public void setPromised(boolean promised) {
		this.promised = promised;
	}
}
