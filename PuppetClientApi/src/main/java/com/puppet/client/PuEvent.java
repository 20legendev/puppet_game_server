package com.puppet.client;

import com.puppet.eventdriven.impl.BaseEvent;

public class PuEvent extends BaseEvent {

	public PuEvent(String type) {
		super(type);
	}

	public PuEvent(String type, Object... keyValueDatas) {
		super(type, keyValueDatas);
	}

	private static final long serialVersionUID = -806977663569160375L;

}
