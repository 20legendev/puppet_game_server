package com.puppet.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import io.netty.channel.DefaultChannelPromise;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.WorkerPool;
import com.puppet.client.events.SocketSendingEvent;
import com.puppet.client.events.SocketSendingEventFactory;
import com.puppet.client.events.SocketSendingEventHandler;
import com.puppet.client.events.SocketSendingEventProducer;
import com.puppet.common.PuEventType;
import com.puppet.common.PuField;
import com.puppet.common.message.PuMessage;
import com.puppet.common.message.concrete.PingMessage;
import com.puppet.eventdriven.EventCallback;
import com.puppet.eventdriven.impl.BaseEvent;
import com.puppet.eventdriven.impl.EventDispatcherImpl;

public class PuppetClient extends EventDispatcherImpl implements ExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(PuppetClient.class);

	private ServerAddress serverAddress;

	private WorkerPool<SocketSendingEvent> workerPool;
	private RingBuffer<SocketSendingEvent> ringBuffer;
	private ExecutorService executor;
	private SocketSendingEventProducer producer;

	private SocketSender socketSender = new SocketSender() {

		@Override
		public void send(byte[] bytes) {
			if (isConnected()) {
				if (bytes != null && bytes.length > 0) {
					ByteBuf bytebuf = Unpooled.buffer(bytes.length);
					bytebuf.writeBytes(bytes);
					channelFuture.channel().writeAndFlush(bytebuf);
				}
			} else {
				throw new RuntimeException("cannot send message while channel has not been connected");
			}
		}

		@Override
		public ChannelPromise sendPromise(byte[] bytes) {
			if (isConnected()) {
				if (bytes != null && bytes.length > 0) {
					ByteBuf bytebuf = Unpooled.buffer(bytes.length);
					bytebuf.writeBytes(bytes);
					ChannelPromise channelPromise = new DefaultChannelPromise(channelFuture.channel());
					channelFuture.channel().writeAndFlush(bytebuf, channelPromise);
					return channelPromise;
				}
				return null;
			} else {
				throw new RuntimeException("cannot send message while channel has not been connected");
			}
		}
	};

	public PuppetClient() {
		this.executor = Executors.newFixedThreadPool(1);

		this.ringBuffer = RingBuffer.createMultiProducer(new SocketSendingEventFactory(this.socketSender), 1024,
				new BlockingWaitStrategy());

		this.workerPool = new WorkerPool<SocketSendingEvent>(this.ringBuffer, this.ringBuffer.newBarrier(), this,
				new SocketSendingEventHandler[] { new SocketSendingEventHandler() });

		this.workerPool.start(this.executor);

		this.producer = new SocketSendingEventProducer(this.ringBuffer);
	}

	public PuppetClient(ServerAddress address) {
		this();
		this.setServerAddress(address);
	}

	public ServerAddress getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(ServerAddress serverAddress) {
		this.serverAddress = serverAddress;
	}

	public void connect() {
		if (this.getServerAddress() == null) {
			throw new RuntimeException("Server address is not defined");
		}
		try {
			this._connect(getServerAddress().getHost(), getServerAddress().getPort(), getServerAddress().isUseSSL());
		} catch (Exception e) {
			this.dispatchEvent(new BaseEvent(PuEventType.CONNECTION_RESPONSE, PuField.SUCCESSFUL, false, PuField.CAUSE,
					e));
		}
	}

	public void connect(String host, int port, boolean useSSL) {
		this.serverAddress = new ServerAddress(host, port, useSSL);
		this.connect();
	}

	private EventLoopGroup group = null;
	private ChannelFuture channelFuture = null;

	private void _connect(String host, int port, boolean useSSL) throws Exception {

		if (this.isConnected()) {
			throw new Exception("connection still alive");
		}

		try {
			final SslContext sslCtx;
			if (PuppetClient.this.getServerAddress().isUseSSL()) {
				sslCtx = SslContext.newClientContext(InsecureTrustManagerFactory.INSTANCE);
			} else {
				sslCtx = null;
			}

			// Configure the client.
			group = new NioEventLoopGroup();
			Bootstrap b = new Bootstrap();
			b.group(group).channel(NioSocketChannel.class).option(ChannelOption.TCP_NODELAY, true)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel ch) throws Exception {
							ChannelPipeline p = ch.pipeline();
							if (sslCtx != null) {
								p.addLast(sslCtx.newHandler(ch.alloc(), getServerAddress().getHost(),
										getServerAddress().getPort()));
							}
							// Decoders
							// p.addLast("frameDecoder", new
							// LengthFieldBasedFrameDecoder(1048576, 0, 4, 0,
							// 4));
							// p.addLast("protobufDecoder", new
							// ProtobufDecoder(MyMessage.getDefaultInstance()));

							// Encoder
							// p.addLast("frameEncoder", new
							// LengthFieldPrepender(4));
							// p.addLast("protobufEncoder", new
							// ProtobufEncoder());

							p.addLast(new MessageReceiver(PuppetClient.this));
						}

						@Override
						public void channelInactive(ChannelHandlerContext ctx) throws Exception {
							forceDisconnect();
						}
					});

			// Start the client.
			channelFuture = b.connect(getServerAddress().getHost(), getServerAddress().getPort());
			channelFuture.syncUninterruptibly();
		} catch (Exception e) {
			PuppetClient.this.dispatchEvent(new BaseEvent(PuEventType.CONNECTION_RESPONSE, PuField.SUCCESSFUL, false,
					PuField.CAUSE, e));
		}
	}

	private void forceDisconnect() {
		try {
			if (this.channelFuture != null) {
				this.channelFuture.channel().closeFuture();
				this.channelFuture = null;
			}
			if (this.group != null) {
				this.group.shutdownGracefully();
				this.group = null;
			}
		} catch (Exception e) {
			throw new RuntimeException("close connection error", e);
		}
		if (this.pingPongTimer != null) {
			this.pingPongTimer.cancel();
			this.pingPongTimer = null;
		}
	}

	public void disconnect() {
		if (this.isConnected()) {
			this.forceDisconnect();
		}
	}

	public void destroy() {
		this.forceDisconnect();
	}

	public boolean isConnected() {
		return channelFuture != null && channelFuture.channel().isActive();
	}

	public void send(PuMessage message) {
		if (this.isConnected()) {
			try {
				byte[] bytes = message.getBytes();
				if (bytes != null && bytes.length > 0) {
					if (message instanceof PingMessage) {
						this.producer.publish(bytes, new EventCallback() {

							@Override
							public void callback(Object data) {
								((PingMessage) message).logSentTime();
							}
						}, true);
					} else {
						this.producer.publish(bytes);
					}
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			throw new RuntimeException("cannot send message while channel has not been connected");
		}
	}

	private Timer pingPongTimer = null;

	/**
	 * automatic send ping message and receive pong, calculate latency time and
	 * dispatch PuEventType.PING_PONG message with "interval" in nano second
	 * 
	 * @param interval
	 *            : time in millisecond to send ping message
	 */
	public void enableLagMonitor(long interval) {
		if (interval > 0) {
			if (interval < 10) {
				logger.warn("", new Exception("interval value is too small, it may raise message broken error"));
			}

			if (pingPongTimer != null) {
				pingPongTimer.cancel();
			}
			pingPongTimer = new Timer();
			pingPongTimer.scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					if (!isConnected()) {
						return;
					}
					send(new PingMessage());
				}
			}, 10, interval);
		} else {
			if (pingPongTimer != null) {
				this.pingPongTimer.cancel();
				this.pingPongTimer = null;
			}
		}
	}

	@Override
	public void handleEventException(Throwable ex, long sequence, Object event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleOnStartException(Throwable ex) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleOnShutdownException(Throwable ex) {
		// TODO Auto-generated method stub

	}
}
