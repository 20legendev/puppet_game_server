package com.puppet.client;

public class ServerAddress {
	private String host;
	private int port;
	private boolean useSSL;

	public ServerAddress(String host, int port) {
		this(host, port, false);
	}

	public ServerAddress(String host, int port, boolean useSSL) {
		this.setHost(host);
		this.setPort(port);
		this.setUseSSL(useSSL);
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isUseSSL() {
		return useSSL;
	}

	public void setUseSSL(boolean useSSL) {
		this.useSSL = useSSL;
	}
}
